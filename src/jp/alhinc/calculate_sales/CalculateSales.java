package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";
	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";
	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";
	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_CONSECUTIVE = "売上ファイル名が連番になっていません";
	private static final String AMOUNT_OVER_10DIGITS = "合計金額が10桁を超えました";
	private static final String INVALID_BRANCHCODE = "の支店コードが不正です";
	private static final String INVALID_COMMCODE = "の商品コードが不正です";
	private static final String INVALID_FORMAT = "のフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */

	public static void main(String[] args) {

		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "^[0-9]{3}$", "支店")) {
			return;
		}
		// 商品定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "^[0-9a-zA-Z]{8}$", "商品")) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)

		// 売上ファイルの読み取り
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		// ファイルがファイル形式でファイル名が数字8桁.rcdであればリストに追加
		for (int i = 0; i < files.length; i++) {
			if (files[i].isFile() && files[i].getName().matches("^[0-9]{8}.(rcd$)")) {
				rcdFiles.add(files[i]);
			}
		}

		// 売上ファイルを昇順に並べる
		Collections.sort(rcdFiles);

		// 売上ファイル名が連番でなければエラーメッセージを表示
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));
			if ((latter - former) != 1) {
				System.out.println(FILE_NOT_CONSECUTIVE);
				return;
			}
		}

		// 売上ファイルの呼び出し
		BufferedReader br = null;
		for (int i = 0; i < rcdFiles.size(); i++) {
			String filesName = rcdFiles.get(i).getName();

			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				// 売上データを入れるリストを作成
				String line;
				List<String> branchRecords = new ArrayList<>();

				// 売上ファイルから売上データを1行ずつ読みリストに追加
				while ((line = br.readLine()) != null) {
					branchRecords.add(line);
				}

				// 支店コード、商品コード、売上金額に変数を与える
				String branchCode = branchRecords.get(0);
				String commodityCode = branchRecords.get(1);
				String sale = branchRecords.get(2);

				// 読み取った売上ファイルのデータの要素が3つでなければエラーメッセージを表示
				if (branchRecords.size() != 3) {
					System.out.print(filesName + INVALID_FORMAT);
					return;
				}

				// 支店定義ファイルの支店コードが売上ファイルの支店コードを含んでいなければエラーメッセージを表示
				if (!branchNames.containsKey(branchCode)) {
					System.out.println(filesName + INVALID_BRANCHCODE);
					return;
				}

				// 商品定義ファイルの商品コードが売上ファイルの商品コードを含んでいなければエラーメッセージを表示
				if (!commodityNames.containsKey(commodityCode)) {
					System.out.println(filesName + INVALID_COMMCODE);
					return;
				}

				// 売上ファイルの売上金額が数字でなければエラーメッセージを表示
				if (!sale.matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				// 売上ファイルの売上金額を取り出してLong型に変換
				long fileSale = Long.parseLong(sale);

				// 売上金額をマップ branchSales に入っている累計金額に加算してから branchSales のマップに戻す
				long branchAmount = branchSales.get(branchCode) + fileSale;

				// 売上金額をマップ commoditySales に入っている累計金額に加算
				long commodityAmount = commoditySales.get(commodityCode) + fileSale;

				// 売上金額が11桁以上であればエラーメッセージを表示
				if (branchAmount >= 10000000000L || commodityAmount >= 10000000000L) {
					System.out.println(AMOUNT_OVER_10DIGITS);
					return;

				}
				// 加算した結果をそれぞれ branchSalesとcommoditySales のマップに戻す
				branchSales.put(branchCode, branchAmount);
				commoditySales.put(commodityCode, commodityAmount);

			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				try {
					if (br != null) {
						// ファイルを閉じる
						br.close();
					}
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		// 商品別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル・商品定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param コードと名前を保持するMap
	 * @param コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names,
			Map<String, Long> sales, String match, String category) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			if (!file.exists()) {
				System.out.println(category + FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {

				String[] items = line.split(",");

				if ((items.length != 2) || (!items[0].matches(match))) {
					System.out.println(category + FILE_INVALID_FORMAT);
					return false;
				}

				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);

			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> branchNames,
			Map<String, Long> branchSales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File outFile = new File(path, fileName);
			FileWriter fileWriter = new FileWriter(outFile);
			bw = new BufferedWriter(fileWriter);

			for (String key : branchNames.keySet()) {

				bw.write(key + "," + branchNames.get(key) + "," + branchSales.get(key));
				bw.newLine();
			}

		} catch (IOException e) {
			System.out.println(e);
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
